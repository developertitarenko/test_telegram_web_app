import './App.css';
import React, {useEffect} from "react";
import Header from "./components/Header/Header";
import {useTelegram} from "./hooks/useTelegram";


import { Routes, Route } from 'react-router-dom'
import ProductList from "./pages/ProductList/ProductList";
import Form from "./pages/Form/Form";



function App() {
  const {tg} = useTelegram()

  useEffect(() => {
    tg.ready()
  } )


  return (
    <div className="App">
      <Header/>
      <Routes>
        <Route index element={<ProductList/>}/>
        <Route path={'/form'} element={<Form/>}/>
      </Routes>
    </div>
  );
}

export default App;
